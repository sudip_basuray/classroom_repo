package a_gradle.testNG;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class loginsuite 
{
	//modified
	WebDriver driver;
	Method_Repository mr1 = new Method_Repository();
	
	@BeforeMethod
	public void appLaunch() throws InterruptedException
	{
		mr1.browserAppLaunch();
	}
	
	/*
	 * TC_001: Verifying valid login functionality
	 */
	@Test(priority = 0, enabled=true, description="TC_001: Verifying valid login functionality")
	public void verifyValidLogin() throws InterruptedException
	{
		try {
				mr1.login("dasd", "dasd");
				Assert.assertEquals(true, mr1.verifyValidLogin());
			}
		catch(Exception e) {
				System.out.println(e);
			}
	}
	
	/*
	 * TC_002: Verifying invalid login functionality
	 */
	@Test(priority = 1, enabled=true, description="TC_002: Verifying invalid login functionality")
	public void verifyInvalidLogin() {
		try {
			mr1.login("dasd1", "dasd1");
			Assert.assertEquals(true, mr1.verifyInValidLogin());
		}
	catch(Exception e) {
			System.out.println(e);
		}
	}
	
	@AfterMethod()
	public void appClose()
	{
		mr1.appClose();
	}
	
}
